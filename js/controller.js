//Reset form
function formRS(){
  document.getElementById("formNV").reset();
}
//render DSNV
function renderDSNV(dsnv){
    contentHTML = "";
    for (var i = 0; i < dsnv.length; i++) {
      var nv = dsnv[i];
      if (nv.XepLoai()==3){
        content=`<tr>
        <td>${nv.tk}</td>
        <td>${nv.hoTen}</td>
        <td>${nv.email}</td>
        <td>${nv.ngayLam}</td>
        <td>${nv.chucVu}</td>
        <td>${nv.TongLuong()}</td>
        <td>Nhân viên xuất sắc</td>
        <td><button data-toggle="modal"
        data-target="#myModal" onclick="suaNhanVien('${nv.tk}')" class="btn btn warning" my-2">Sửa</button> 
        <button onclick="xoaNhanVien('${nv.tk}')" class="btn btn-danger">Xoá</button>
        </td>
        </tr>`
      }
      else if (nv.XepLoai()==2){
        content=`<tr>
        <td>${nv.tk}</td>
        <td>${nv.hoTen}</td>
        <td>${nv.email}</td>
        <td>${nv.ngayLam}</td>
        <td>${nv.chucVu}</td>
        <td>${nv.TongLuong()}</td>
        <td>Nhân viên giỏi</td>
        <td><button data-toggle="modal"
        data-target="#myModal" onclick="suaNhanVien('${nv.tk}')" class="btn btn warning" my-2">Sửa</button> 
        <button onclick="xoaNhanVien('${nv.tk}')" class="btn btn-danger">Xoá</button>
        </td>
        </tr>`
      }
      else if (nv.XepLoai()==1){
        content=`<tr>
        <td>${nv.tk}</td>
        <td>${nv.hoTen}</td>
        <td>${nv.email}</td>
        <td>${nv.ngayLam}</td>
        <td>${nv.chucVu}</td>
        <td>${nv.TongLuong()}</td>
        <td>Nhân viên khá</td>
        <td><button data-toggle="modal"
        data-target="#myModal" onclick="suaNhanVien('${nv.tk}')" class="btn btn warning" my-2">Sửa</button> 
        <button onclick="xoaNhanVien('${nv.tk}')" class="btn btn-danger">Xoá</button>
        </td>
        </tr>`
      }
        else{
            content=`<tr>
            <td>${nv.tk}</td>
            <td>${nv.hoTen}</td>
            <td>${nv.email}</td>
            <td>${nv.ngayLam}</td>
            <td>${nv.chucVu}</td>
            <td>${nv.TongLuong()}</td>
            <td>Nhân viên trung bình</td>
            <td><button data-toggle="modal"
            data-target="#myModal" onclick="suaNhanVien('${nv.tk}')" class="btn btn warning" my-2">Sửa</button> 
            <button onclick="xoaNhanVien('${nv.tk}')" class="btn btn-danger">Xoá</button>
            </td>
            </tr>`
        }
      
      contentHTML+=content;
    }
    document.getElementById("tableDanhSach").innerHTML=contentHTML;  
}

function layThongTinTuForm(){
  var tk = document.getElementById("tknv").value;
  var hoTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCB = Number(document.getElementById("luongCB").value);
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = Number(document.getElementById("gioLam").value);
  return new NhanVien(tk,hoTen,email,matKhau,ngayLam,luongCB,chucVu,gioLam);
};
//Lưu data xuống local
function luuData(dsnv){
    var dataJSON=JSON.stringify(dsnv);
    localStorage.setItem("DSNV",dataJSON);
}
//Show thông tin lên form
function showThongTinLenForm(sv){
   document.getElementById("tknv").value=sv.tk;
   document.getElementById("name").value=sv.hoTen;
   document.getElementById("email").value=sv.email;
   document.getElementById("password").value=sv.matKhau;
   document.getElementById("datepicker").value=sv.ngayLam;
   document.getElementById("luongCB").value=sv.luongCB;
   document.getElementById("chucvu").value=sv.chucVu;
   document.getElementById("gioLam").value=sv.gioLam;
}
function kiemTraForm(nv){
    //Ưu tiên validate để trống trước
    //Validate tài khoản
    var isValid = kiemTraTrong(nv.tk,"tbTKNV")&&kiemTraDoDai(6,4,"tbTKNV",nv.tk,"Vui lòng nhập từ 4-6 kí số")&&kiemTraLaSo(nv.tk,"tbTKNV")&&kiemTraKhoangCach(nv.tk,"tbTKNV");
  
    //Validate tên nhân viên
    isValid &= kiemTraTrong(nv.hoTen,"tbTen") &&kiemTraTen(nv.hoTen);
  
    //Validate email
    isValid &=kiemTraTrong(nv.email,"tbEmail")&& kiemTraEmail(nv.email)&&kiemTraKhoangCach(nv.email,"tbEmail");
  
    //Validate mật khẩu
    isValid &= kiemTraTrong(nv.matKhau,"tbMatKhau") && kiemTraDoDai(10,6,"tbMatKhau",nv.matKhau,"Mật khẩu từ 6-10 kí tự") && kiemTraMatKhau(nv.matKhau)&&kiemTraKhoangCach(nv.matKhau,"tbMatKhau");
  
    //Validate ngày
    isValid &= kiemTraTrong(nv.ngayLam,"tbNgay") && kiemTraNgay(nv.ngayLam);
  
    //Validate lương CB
    isValid &= kiemTraTrong(nv.luongCB,"tbLuongCB") && kiemTraMucGiaTri(20e+6,1e+6,"tbLuongCB",nv.luongCB,"Mức lương CB từ 1 triệu tới 6 triệu");
  
    //Validate chức vụ
    isValid &= kiemTraTrong(nv.chucVu,"tbChucVu");
    
     //Validate số giờ làm
    isValid &= kiemTraTrong(nv.gioLam,"tbGiolam") && kiemTraMucGiaTri(200,80,"tbGiolam",nv.gioLam,"Giới hạn giờ làm từ 80 - 200");
     return isValid;
}
//Có một số cái khi không truyền vào hàm thì hàm vẫn biết và xử lí được, một số cái thì phải có tham số và truyền vào thì hàm mới chịu xử lí, tại sao.
