//Tạo mảng rỗng dùng khi load trang và lưu dữ liệu khi thêm sv
var dsnv = [];
//Xử lí khi load trang
var dataJSON=localStorage.getItem("DSNV");
if (dataJSON!=null){
    dsnv=JSON.parse(dataJSON).map(function(item){
        return new NhanVien(item.tk,item.hoTen,item.email,item.matKhau,item.ngayLam,item.luongCB,item.chucVu,item.gioLam);
    });
    renderDSNV(dsnv);
}

//Thêm nhân viên
function themNhanVien() {
  var nv = layThongTinTuForm();
  var isValid = kiemTraForm(nv);
  console.log(isValid);
  if(isValid){
  dsnv.push(nv);
  //render dsnv lên table
  renderDSNV(dsnv);
  var dataJSON=JSON.stringify(dsnv);
  localStorage.setItem("DSNV",dataJSON);
  formRS();
  //Nhớ lưu xuống local storage là phải là dataJSON chứ k phải dsnv!!!!!
}}

//Xoá nhân viên
function xoaNhanVien(id){
    var index=dsnv.findIndex(function(item){
       return item.tk==id;
    });
    dsnv.splice(index,1);
    renderDSNV(dsnv);
    luuData(dsnv);
}

//Sửa nhân viên
function suaNhanVien(id){
    var index=dsnv.findIndex(function(item){
        return item.tk==id;
    });
    showThongTinLenForm(dsnv[index]);
}
 
//Cập nhật sinh viên
function capNhatNhanVien(){
    var nv=layThongTinTuForm(); //Do nv là biến khai báo trong hàm nên khi tách hàm kiemTraForm bên controller thì phải truyền tham số để khi sử dụng bên main nó biết lấy giá trị, còn nếu nv là biến toàn cục (khai báo ngoài hàm hoặc nếu biến đó được sử dụng trong hàm trước thì cũng là thay đổi giá trị) thì có thể tách hàm không cần truyền tham số nv thì hàm layThongTinTuForm nó vẫn biết là lấy giá trị biến nv toàn cục.
    var isValid = kiemTraForm(nv);
    if (isValid){
    var index=dsnv.findIndex(function(item){
        return item.tk==nv.tk;
    });
    dsnv[index]=nv;
    renderDSNV(dsnv);
    luuData(dsnv);
    formRS();
}
}
function timKiemNhanVien(){
    var value=document.getElementById("searchName").value;
    var dsxl=[];
    for (var i=0;i<dsnv.length;i++){
        var nv=dsnv[i];
        if (nv.XepLoai()==value){
            dsxl.push(dsnv[i]);
        }
        renderDSNV(dsxl);
    }
}
document.getElementById('btnTimNV').addEventListener('click',timKiemNhanVien);
