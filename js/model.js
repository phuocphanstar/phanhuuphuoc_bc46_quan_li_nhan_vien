function NhanVien(tk, hoTen, email, matKhau, ngayLam, luongCB, chucVu, gioLam) {
  this.tk = tk;
  this.hoTen = hoTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCB = luongCB;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.TongLuong = function () {
    var tongluong;
    if (this.chucVu == "Giám Đốc") {
      tongluong = this.luongCB * 3;
      return tongluong;
    } else if (this.chucVu == "Trưởng Phòng") {
      tongluong = this.luongCB * 2;
      return tongluong;
    } else {
      tongluong = this.luongCB;
      return tongluong;
    }
  };
  this.XepLoai = function () {
    var rankXL;
    if (this.gioLam >= 192) {
      return rankXL=3;//Nhân viên xuất sắc
    } else if (this.gioLam >= 176) {
      return rankXL=2;//Nhân viên giỏi
    } else if (this.gioLam >= 160) {
      return rankXL=1; //Nhân viên khá
    } else {
      return rankXL=0; //Nhân viên trung bình
    }
  };
}
